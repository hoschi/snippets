.PHONY: help
.SILENT: help

help: # print this message
	echo -e "Available commands"
	echo -e "------------------"
	grep ^\[A-Za-z] Makefile | sed 's/:.*#\s*/:#/' | column -t -s '#'


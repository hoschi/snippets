#!/bin/bash
# Quick fix, for when a snap only shows blank/questionable characters
# in one of it's dialoges.
fc-cache -r

# and/or manually delete fontconfig cache directory in home:
# rm ~/.cache/fontconfig/*

# And possibly the system cache too:
# sudo rm -f /var/cache/fontconfig/*

# snippets

Everything that does not deserve a dedicated repository.

## Usage

To download single snippets use [snippet-dl](./snippet-dl).
Of course you will need to manually download this one first.

## Disclaimer
No maintainance or support intended. Expect to find bug and vulnerabilities.
Some files are more useful than others.

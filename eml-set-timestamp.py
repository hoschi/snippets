#!/usr/bin/env nix-shell
#! nix-shell -p python3 -i python3
#
# set modification dates of all eml files in current directory and
# subdirectories to the emails date
#
# side effects:
# - remove some non-alphanumeric characters from filenames => rename files

import os
import sys


def eml_get_date(filepath):
  cmd = f"date --iso=s --date=\"$(cat '{filepath}' | grep --max-count=1 ^Date: | sed 's/Date: //')\""
  process = os.popen(cmd)
  date = process.read()
  process.close()
  return date


def file_set_date(file, date):
  cmd = f"touch --date=\"{date}\" {file}"
  exit_code = os.system(cmd)
  assert exit_code == 0, f"file: {file}"


def cleanup_filename(path):
  import re
  new_filepath = re.sub(r'[^a-zA-Z0-9:_./-]', '-', path)
  os.renames(path, new_filepath)
  return new_filepath


def handle_file(path):
  path = cleanup_filename(path)
  date = eml_get_date(path)
  file_set_date(path, date)
  ## for debugging
  # print(f"{path}: success")


def main():
  import glob

  dir_ = "."
  files = glob.glob(f"{dir_}/**/*.eml", recursive=True)

  for filepath in files:
    handle_file(filepath)


main()
